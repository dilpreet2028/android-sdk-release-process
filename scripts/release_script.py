import os
import gitlab

GITLAB_TOKEN = os.getenv('GITLAB_CI_TOKEN', '')

gl = gitlab.Gitlab('https://gitlab.com/', private_token=GITLAB_TOKEN)
gl.auth()

project_id = os.getenv('CI_PROJECT_ID', '')
project = gl.projects.get(project_id)

#getting the current milestone
p_milestones = project.milestones.list(state='active')
milestone = p_milestones[0].title

# Extracting the milestone into sdk version:   v0.0.1 -> 0.0.1
version = milestone.split('v')[1]

## create release here
os.system("./gradlew clean assembleRelease --stacktrace")
os.system("./gradlew publish -Pversion={0} --stacktrace".format(version))


# set close state the current milestone
p_milestones[0].state_event = 'close'

# create a new milestone
newVersion = 'v0.0.' + str(int(milestone.split('.')[2]) + 1)
project.milestones.create({'title': newVersion})

# close previous milestone
p_milestones[0].save()