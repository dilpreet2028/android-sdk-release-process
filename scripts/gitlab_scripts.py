import os
import gitlab

### Will be triggered for all MR
###  1. Get the latest milestone
###  3. Create a git tag
GITLAB_TOKEN = os.getenv('GITLAB_CI_TOKEN', '')

gl = gitlab.Gitlab('https://gitlab.com/', private_token=GITLAB_TOKEN)
gl.auth()

# Geting the current project
project_id = os.getenv('CI_PROJECT_ID', '')
project = gl.projects.get(project_id)

print(os.getenv('CI_COMMIT_TITLE', 'NULL'))

#Search the MR from the commit title we got
commits = project.search('commits', os.getenv('CI_COMMIT_TITLE', 'NULL'))
if len (commits) > 0:
    commitId = commits[0]['short_id']
    mr = project.commits.get(commitId).merge_requests()

    #checking whether release label is present on the mr
    if len(mr) > 0 and len(mr[0]['labels']) > 0 and 'RELEASE' in mr[0]['labels']:
        p_milestones = project.milestones.list(state='active')

        # Create a release description for that particular milestone
        release_desc = ""
        milestone = p_milestones[0].title
        for mr in p_milestones[0].merge_requests():
            release_desc = release_desc + " " + mr.sha + " " + mr.title + "<br>"

        #create a git tag for the current milestone number
        project.tags.create({'tag_name': milestone, 'ref': 'master'})

        #creating a new release for the milestone
        project.releases.create({'name': milestone, 'tag_name': milestone, 'description': release_desc})
    else:
        print("Release label is not present, skipping release process.")
else:
    print("No commits found")
