package com.dilpreet2028.showcasepipelines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dilpreet2028.secretsdk.SecretSdk

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sdk = SecretSdk()
        sdk.doStuff()
    }
}